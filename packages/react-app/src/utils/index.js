import awardsOne from "../assets/awardsOne.png";
import awardsTwo from "../assets/awardsTwo.jpeg";
import awardsThree from "../assets/awardsThree.jpeg";
import awardsFour from "../assets/awardsFour.png";
import feedOne from "../assets/feedOne.jpeg";
import feedTwo from "../assets/feedTwo.jpeg";
import feedThree from "../assets/feedThree.webp";
import project from "../assets/project.webp";
import avatar from "../assets/avatar.png";

export const ProjectData = [
  {
    id: 1,
    title: "Name",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    image: project,
    avatar: avatar,
    time: "6 months ago",
  },
  {
    id: 2,
    title: "Name",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    image: project,
    avatar: avatar,
    time: "3 months ago",
  },
  {
    id: 3,
    title: "Name",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    image: project,
    avatar: avatar,
    time: "1 month ago",
  },
];

export const FeedData = [
  {
    id: 1,
    title: "Name",
    content: "example content",
    image: feedOne,
    avatar: avatar,
    time: "6 months ago",
  },
  {
    id: 2,
    title: "Name",
    content: "example content",
    image: feedTwo,
    avatar: avatar,
    time: "3 months ago",
  },
  {
    id: 3,
    title: "Name",
    content: "example content",
    image: feedThree,
    avatar: avatar,
    time: "1 month ago",
  },
];

export const AwardsData = [
  {
    id: 1,
    title: "Buff Blaze",
    content: "Lorem ipsum dolor sit amet",
    image: awardsFour,
  },
  {
    id: 2,
    title: "Buff Blaze",
    content: "Lorem ipsum dolor sit amet",
    image: awardsFour,
  },
  {
    id: 3,
    title: "Buff Blaze",
    content: "Lorem ipsum dolor sit amet",
    image: awardsFour,
  },
];

export const itemData = [
  {
    img: "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
    title: "Breakfast",
    author: "@bkristastucchio",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    title: "Burger",
    author: "@rollelflex_graphy726",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    title: "Camera",
    author: "@helloimnik",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    title: "Coffee",
    author: "@nolanissac",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
    title: "Hats",
    author: "@hjrc33",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
    title: "Honey",
    author: "@arwinneil",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6",
    title: "Basketball",
    author: "@tjdragotta",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1518756131217-31eb79b20e8f",
    title: "Fern",
    author: "@katie_wasserman",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1597645587822-e99fa5d45d25",
    title: "Mushrooms",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1567306301408-9b74779a11af",
    title: "Tomato basil",
    author: "@shelleypauls",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1471357674240-e1a485acb3e1",
    title: "Sea star",
    author: "@peterlaster",
    avatar: avatar,
  },
  {
    img: "https://images.unsplash.com/photo-1589118949245-7d38baf380d6",
    title: "Bike",
    author: "@southside_customs",
    avatar: avatar,
  },
];
