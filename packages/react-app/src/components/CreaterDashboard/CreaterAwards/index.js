import React, { useState } from "react";
import Box from "@mui/material/Box";
import { AwardsData } from "../../../utils";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import {
  FeedTitle,
  FeedTime,
  FeedIconButton,
  VideoTitle,
  VideoParagraph,
} from "../../index";
function CreaterAwards() {
  return (
    <Box display="grid" gridTemplateColumns="repeat(3, 1fr)" gap={2}>
      {AwardsData.map((item) => (
        <Card
          key={item.id}
          style={{ background: "#eff7fc" }}
          sx={{ maxWidth: 345 }}
        >
          <CardMedia
            component="img"
            alt="green iguana"
            height="300"
            image={item.image}
            style={{ width: "21.5rem", height: "21.5rem", padding: "2rem" }}
          />
          <CardContent style={{ textAlign: "center" }}>
            <VideoTitle style={{ margin: 0 }} gutterBottom component="div">
              {item.title}
            </VideoTitle>
            <VideoParagraph
              style={{ width: "auto", textAlign: "inherit" }}
              variant="body2"
              color="text.secondary"
            >
              {item.content}
            </VideoParagraph>
          </CardContent>
          <CardActions
            style={{ justifyContent: "center", marginBottom: "0.7rem" }}
          >
            <Button variant="contained">Claim</Button>
          </CardActions>
        </Card>
      ))}
    </Box>
  );
}

export default CreaterAwards;
