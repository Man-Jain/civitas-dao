import React, { useState } from "react";
import { NewBox, NewField, VideoTitle } from "../../index";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";

function CreaterNew() {
  const Input = styled("input")({
    display: "none",
  });
  return (
    <NewBox
      sx={{
        width: 500,
        maxWidth: "100%",
      }}
    >
      <VideoTitle style={{ margin: 0 }} gutterBottom component="div">
        Add Title
      </VideoTitle>
      <NewField fullWidth label="Title" id="fullWidth" />
      <NewField fullWidth label="Content" id="fullWidth" />
      <div style={{ marginTop: "2rem", textAlign: "center" }}>
        <label htmlFor="contained-button-file">
          <Input
            accept="image/*"
            id="contained-button-file"
            multiple
            type="file"
          />
          <Button variant="contained" component="span">
            Upload
          </Button>
        </label>
        <Button variant="contained" style={{ marginLeft: "2rem" }}>
          Submit
        </Button>
      </div>
    </NewBox>
  );
}

export default CreaterNew;
