import React, { useState } from "react";
import { FeedTitle, FeedTime, FeedIconButton } from "../../index";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";

import { FeedData } from "../../../utils";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import ShareIcon from "@mui/icons-material/Share";
import PlayCircleOutlineIcon from "@mui/icons-material/PlayCircleOutline";
function CreaterHome() {
  const navigate = useNavigate();

  function handleVideoClick() {
    navigate("/video");
  }
  return (
    <Box display="grid" gridTemplateColumns="repeat(1, 1fr)" gap={2}>
      {FeedData.map((item) => (
        <Card
          key={item.id}
          style={{ margin: "2rem 15rem", background: "#eff7fc" }}
        >
          <FeedIconButton onClick={handleVideoClick} color="inherit">
            <PlayCircleOutlineIcon fontSize="large" />
          </FeedIconButton>
          <CardMedia
            component="img"
            alt="green iguana"
            height="300"
            image={item.image}
          />
          <CardContent style={{ paddingBottom: "0px" }}>
            <Grid container spacing={0}>
              <Grid item xs={1}>
                <Avatar alt="Remy Sharp" src={item.avatar} />
              </Grid>
              <Grid item xs={1}>
                <FeedTitle gutterBottom variant="h5" component="div">
                  {item.title}
                </FeedTitle>
              </Grid>
              <Grid item xs={1}>
                <FeedTime gutterBottom variant="h6" component="div">
                  {item.time}
                </FeedTime>
              </Grid>
              <Grid item xs={9}>
                <IconButton
                  style={{ float: "right" }}
                  onClick={handleVideoClick}
                  color="inherit"
                >
                  <ShareIcon />
                </IconButton>
              </Grid>
            </Grid>
          </CardContent>
          {/* <CardActions style={{ justifyContent: "flex-end" }}>
            <Button size="small">Share</Button>
            <Button size="small">Learn More</Button>
          </CardActions> */}
        </Card>
      ))}
    </Box>
  );
}

export default CreaterHome;
