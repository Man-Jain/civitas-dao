import React, { useState } from "react";
import Box from "@mui/material/Box";
import {
  FeedTitle,
  FeedTime,
  FeedIconButton,
  ProjectParagraph,
} from "../../index";
import { ProjectData } from "../../../utils";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import ShareIcon from "@mui/icons-material/Share";
import IconButton from "@mui/material/IconButton";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
function CreaterProjects() {
  return (
    <Box display="grid" gridTemplateColumns="repeat(3, 1fr)" gap={2}>
      {ProjectData.map((item) => (
        <Card
          style={{ background: "#eff7fc" }}
          key={item.id}
          sx={{ maxWidth: 345 }}
        >
          <CardMedia
            component="img"
            alt="green iguana"
            height="250"
            image={item.image}
          />
          <CardContent>
            <Grid container spacing={0}>
              <Grid item xs={2}>
                <Avatar alt="Remy Sharp" src={item.avatar} />
              </Grid>
              <Grid item xs={4}>
                <FeedTitle
                  style={{ margin: "0px" }}
                  gutterBottom
                  variant="h5"
                  component="div"
                >
                  {item.title}
                </FeedTitle>
                <FeedTime
                  style={{ margin: "0px" }}
                  gutterBottom
                  variant="h6"
                  component="div"
                >
                  {item.time}
                </FeedTime>
              </Grid>

              <Grid item xs={6}>
                <IconButton style={{ float: "right" }} color="inherit">
                  <ShareIcon />
                </IconButton>
              </Grid>
            </Grid>
            <ProjectParagraph variant="body2" color="text.secondary">
              {item.content}
            </ProjectParagraph>
          </CardContent>
        </Card>
      ))}
    </Box>
  );
}

export default CreaterProjects;
