import { IconButton, TextField } from "@mui/material";
import { style } from "@mui/system";
import styled from "styled-components";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Drawer } from "../components/Drawer";
export const Body = styled.div`
  align-items: center;
  color: white;
  display: flex;
  flex-direction: column;
  font-size: calc(10px + 2vmin);
  justify-content: center;
  margin-top: 40px;
`;

export const Button = styled.button`
  background-color: white;
  border: none;
  border-radius: 8px;
  color: #282c34;
  cursor: pointer;
  font-size: 16px;
  margin: 0px 20px;
  padding: 12px 24px;
  text-align: center;
  text-decoration: none;
`;

export const Container = styled.div`
  background-color: #282c34;
  display: flex;
  flex-direction: column;
  height: calc(100vh);
`;

export const Header = styled.header`
  align-items: center;
  background-color: #282c34;
  color: white;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  min-height: 70px;
`;

export const Image = styled.img`
  height: 40vmin;
  margin-bottom: 16px;
  pointer-events: none;
`;

export const Link = styled.a.attrs({
  target: "_blank",
  rel: "noopener noreferrer",
})`
  color: #61dafb;
  margin-top: 8px;
`;
export const FeedTitle = styled.h5`
  font-size: 16px;
  color: #2f4858;
  margin: 12px 0px;
`;
export const FeedTime = styled.h6`
  margin: 18px 0px;
  color: #6a6666;
  font-size: 9px;
`;
export const FeedIconButton = styled(IconButton)`
  &.css-zylse7-MuiButtonBase-root-MuiIconButton-root {
    position: absolute;
    width: 63%;
    height: 38%;
    cursor: pointer;
    &:hover {
      background-color: transparent;
    }
  }
`;
export const VideoTitle = styled.h3`
  margin: 7px 15px;
  font-size: 20px;
`;
export const VideoParagraph = styled(Typography)`
  margin-top: 1.5rem;
  text-align: justify;
  width: 1000px;
`;
export const ProjectParagraph = styled(Typography)`
  margin-top: 1rem;
  margin-left: 0.3rem;
  text-align: justify;
`;
export const MenuIconButton = styled(IconButton)`
  flex-grow: 1;
  &.css-p55zm9-MuiButtonBase-root-MuiIconButton-root {
    justify-content: left;
    flex-grow: 1;
    &:hover {
      background-color: transparent;
    }
  }
`;
export const NewBox = styled(Box)`
  &.css-1gyr8kh {
    width: 900px;
    max-width: 100%;
    margin: 2rem 15rem;
    border: 1px solid #eeeeee;
    padding: 2rem 3rem;
  }
`;
export const NewField = styled(TextField)`
  &.css-wb57ya-MuiFormControl-root-MuiTextField-root {
    margin-top: 2rem;
  }
`;
export const MenuDrawer = styled(Drawer)`
  &.css-12i7wg6-MuiPaper-root-MuiDrawer-paper {
    background-color: #eff7fc;
  }
`;
