import erc20Abi from "./abis/erc20.json";
import ownableAbi from "./abis/ownable.json";
import lensHubProxyAbi from "./abis/lensHubProxy.json";
import mockProfileCreationProxyAbi from "./abis/mockProfileCreationProxy.json";

const abis = {
  erc20: erc20Abi,
  ownable: ownableAbi,
  lensHubProxy: lensHubProxyAbi,
  mockProfileCreationProxy: mockProfileCreationProxyAbi,
};

export default abis;
